//
//  BoardTests.swift
//  ChessTests
//
//  Created by Binoy Vijayan on 4/2/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import XCTest

class BoardTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func testLeft(){
        
        let boxA1 = Square(id: ("a",1), color: .chessBlack)
        XCTAssertNil(boxA1.left)
        
        let boxA8 = Square(id: ("a",8), color: .chessBlack)
        XCTAssertNil(boxA8.left)
        
        let boxB1 = Square(id: ("b",1), color: .chessBlack)
        XCTAssertTrue(boxB1.left! == ("A" , 1))
    }
    
    func testRight(){
       
        let boxH1 = Square(id: ("h",1), color: .chessBlack)
        XCTAssertNil(boxH1.right)
        
        let boxH8 = Square(id: ("h",8), color: .chessBlack)
        XCTAssertNil(boxH8.right)

        
        let boxG1 = Square(id: ("g",1), color: .chessBlack)
        XCTAssertTrue(boxG1.right! == ("H" , 1))
    }
    
    func testUp(){
        
        let boxA8 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA8.up)
        
        let boxH8 = Square(id: ("H",8), color: .chessBlack)
        XCTAssertNil(boxH8.up)

        
        let boxA7 = Square(id: ("A",7), color: .chessBlack)
        XCTAssertTrue(boxA7.up! == ("A" , 8))
    }
    
    func testDown(){
        
        let boxA1 = Square(id: ("A",1), color: .chessBlack)
        XCTAssertNil(boxA1.down)
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.down)
        
        let boxA2 = Square(id: ("A",2), color: .chessBlack)
        XCTAssertTrue(boxA2.down! == ("A" , 1))
    }
    
    func testUpLeft(){
        
        let boxA1 = Square(id: ("A",1), color: .chessBlack)
        XCTAssertNil(boxA1.upLeft)
        
        let boxA8 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA8.upLeft)
        
        let boxA7 = Square(id: ("A",7), color: .chessBlack)
        XCTAssertNil(boxA7.upLeft)
        
        let boxB8 = Square(id: ("B",8), color: .chessBlack)
        XCTAssertNil(boxB8.upLeft)
        
        let boxH8 = Square(id: ("H",8), color: .chessBlack)
        XCTAssertNil(boxH8.upLeft)
        
        
        let boxB1 = Square(id: ("B",1), color: .chessBlack)
        XCTAssertTrue(boxB1.upLeft! == ("A" , 2))
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertTrue(boxB7.upLeft! == ("A" , 8))

    }
    
    func testUpRight(){
        
        let boxA8 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA8.upRight)
        
        let boxH8 = Square(id: ("H",8), color: .chessBlack)
        XCTAssertNil(boxH8.upRight)
        
        let boxH7 = Square(id: ("H",7), color: .chessBlack)
        XCTAssertNil(boxH7.upRight)
        
        
        let boxB8 = Square(id: ("B",8), color: .chessBlack)
        XCTAssertNil(boxB8.upRight)
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertTrue(boxB7.upRight! == ("C" , 8))
        
    }
    
    func testDownLeft(){
        
        let boxA1 = Square(id: ("A",1), color: .chessBlack)
        XCTAssertNil(boxA1.downLeft)
        
        let boxA8 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA8.downLeft)
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.downLeft)
        
        let boxB1 = Square(id: ("B",1), color: .chessBlack)
        XCTAssertNil(boxB1.downLeft)
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertTrue(boxB7.downLeft! == ("A" , 6))
        
    }
    
    func testDownRight(){
        
        let boxA1 = Square(id: ("A",1), color: .chessBlack)
        XCTAssertNil(boxA1.downRight)
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.downRight)
        
        let boxH8 = Square(id: ("H",8), color: .chessBlack)
        XCTAssertNil(boxH8.downRight)
        
        let boxA8 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertTrue(boxA8.downRight! == ("B" , 7))
        
    }
    
    func testKtUpLeft(){
        
        let boxA1 = Square(id: ("A",1), color: .chessBlack)
        XCTAssertNil(boxA1.ktUpLeft)
        
        let boxA7 = Square(id: ("A",7), color: .chessBlack)
        XCTAssertNil(boxA7.ktUpLeft)
        
        let boxA6 = Square(id: ("A",6), color: .chessBlack)
        XCTAssertNil(boxA6.ktUpLeft)
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertNil(boxB7.ktUpLeft)
        
        let boxH7 = Square(id: ("H",7), color: .chessBlack)
        XCTAssertNil(boxH7.ktUpLeft)
        
        let boxB6 = Square(id: ("B",6), color: .chessBlack)
        XCTAssertTrue(boxB6.ktUpLeft! == ("A" , 8))
        
    }
    
    func testKtUpRight(){
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.ktUpRight)
        
        let boxH7 = Square(id: ("H",7), color: .chessBlack)
        XCTAssertNil(boxH7.ktUpRight)
        
        let boxA7 = Square(id: ("A",7), color: .chessBlack)
        XCTAssertNil(boxA7.ktUpRight)
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertNil(boxB7.ktUpRight)
        
        let boxG6 = Square(id: ("G",6), color: .chessBlack)
        XCTAssertTrue(boxG6.ktUpRight! == ("H" , 8))
        
    }
    
    func testKtRightUp(){
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.ktRightUp)
        
        let boxG1 = Square(id: ("G",1), color: .chessBlack)
        XCTAssertNil(boxG1.ktRightUp)
        
        let boxF8 = Square(id: ("F",8), color: .chessBlack)
        XCTAssertNil(boxF8.ktRightUp)
        
        let boxA8 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA8.ktRightUp)
        
        let boxF7 = Square(id: ("F",7), color: .chessBlack)
        XCTAssertTrue(boxF7.ktRightUp! == ("H" , 8))
        
    }
    
    func testKtRightDown(){
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.ktRightDown)
        
        let boxG1 = Square(id: ("G",1), color: .chessBlack)
        XCTAssertNil(boxG1.ktRightDown)
        
        let boxF1 = Square(id: ("F",1), color: .chessBlack)
        XCTAssertNil(boxF1.ktRightDown)
        
        let boxG2 = Square(id: ("G",2), color: .chessBlack)
        XCTAssertNil(boxG2.ktRightDown)
        
        let boxG3 = Square(id: ("G",3), color: .chessBlack)
        XCTAssertNil(boxG3.ktRightDown)
        
        let boxF3 = Square(id: ("F",2), color: .chessBlack)
        XCTAssertTrue(boxF3.ktRightDown! == ("H" , 1))
    }
    
    func testKtDownRight(){
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.ktDownRight)
        
        let boxG1 = Square(id: ("G",1), color: .chessBlack)
        XCTAssertNil(boxG1.ktDownRight)
        
        let boxA2 = Square(id: ("A",2), color: .chessBlack)
        XCTAssertNil(boxA2.ktDownRight)
        
        let boxH2 = Square(id: ("H",2), color: .chessBlack)
        XCTAssertNil(boxH2.ktDownRight)
        
        let boxG3 = Square(id: ("G",3), color: .chessBlack)
        XCTAssertTrue(boxG3.ktDownRight! == ("H" , 1))
       
    }
    
    func testKtDownLeft(){
        
        let boxH1 = Square(id: ("H",1), color: .chessBlack)
        XCTAssertNil(boxH1.ktDownLeft)
        
        let boxA1 = Square(id: ("A",1), color: .chessBlack)
        XCTAssertNil(boxA1.ktDownLeft)
        
        let boxA2 = Square(id: ("A",2), color: .chessBlack)
        XCTAssertNil(boxA2.ktDownLeft)
        
        let boxB2 = Square(id: ("B",2), color: .chessBlack)
        XCTAssertNil(boxB2.ktDownLeft)
        
        let boxB3 = Square(id: ("B",3), color: .chessBlack)
        XCTAssertTrue(boxB3.ktDownLeft! == ("A" , 1))
    }
    
    func testKtLeftUp(){
        
        let boxA1 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA1.ktLeftUp)
        
        let boxB1 = Square(id: ("B",8), color: .chessBlack)
        XCTAssertNil(boxB1.ktLeftUp)
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertNil(boxB7.ktLeftUp)
        
        let boxC8 = Square(id: ("C",8), color: .chessBlack)
        XCTAssertNil(boxC8.ktLeftUp)
        
        let boxC7 = Square(id: ("C",7), color: .chessBlack)
        XCTAssertTrue(boxC7.ktLeftUp! == ("A" , 8))
    }
    
    func testKtLeftDown(){
        
        let boxA1 = Square(id: ("A",8), color: .chessBlack)
        XCTAssertNil(boxA1.ktLeftDown)
        
        let boxB1 = Square(id: ("B",8), color: .chessBlack)
        XCTAssertNil(boxB1.ktLeftDown)
        
        let boxB7 = Square(id: ("B",7), color: .chessBlack)
        XCTAssertNil(boxB7.ktLeftDown)
        
        let boxC1 = Square(id: ("C",1), color: .chessBlack)
        XCTAssertNil(boxC1.ktLeftDown)
        
        let boxC2 = Square(id: ("C",2), color: .chessBlack)
        XCTAssertTrue(boxC2.ktLeftDown! == ("A" , 1))
    }
    
    func testBoar(){
        
        let board = Board()
        
        XCTAssertNil(board)
    }
    
}
