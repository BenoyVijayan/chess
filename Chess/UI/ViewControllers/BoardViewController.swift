//
//  BoardViewController.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/3/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit

class BoardViewController: UIViewController {

    @IBOutlet weak var boardView:BoardView!
    private var setupDone:Bool = false
    static var instance:BoardViewController{
        get{
            return BoardViewController(nibName: "BoardViewController", bundle: nil)
        }
    }
    
    @IBAction func rotate( _ sender:UIButton){
       
        boardView.rotate()
    }
    
    override func viewDidLayoutSubviews() {
        
        if !setupDone{
            boardView.calculateThePointsForSquars()
            setupDone = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
