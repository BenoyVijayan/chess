//
//  PieceView.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/4/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit

class PieceView: UIImageView {

    var piece:Piece? = nil
    private var orientation:CGFloat = 1.0
    
    var disable :Bool = false
    
    var panGesture:UIPanGestureRecognizer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
    }
    
    convenience init(frame: CGRect , piece:Piece){
        
        let nwFrame = CGRect( x:frame.origin.x + 6 ,y:frame.origin.y + 6,width: frame.size.width - 12 , height:frame.size.height - 12)

        self.init(frame: nwFrame)
        self.piece = piece
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(PieceView.handlePan(_:)))
        panGesture!.minimumNumberOfTouches = 1
        panGesture!.maximumNumberOfTouches = 1
        self.addGestureRecognizer(panGesture!)
        
        piece.disablePieceView = { disable in
            DispatchQueue.main.async {
                self.disable = disable
                
                print("\(self.piece!.pieceId) : \(self.disable)")
            }
            
        }
        
        if piece.direction == .upside{
            self.disable = false
        }else{
            self.disable = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func rotate(){
        self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        orientation = -1.0
    }
    
    func identity(){
        self.transform = CGAffineTransform.identity
        orientation = 1.0
    }
    
    func draggView(_ recognizer: UIPanGestureRecognizer){
        
        if self.disable == true{
            return
        }
        let translation = recognizer.translation(in: self)
        recognizer.setTranslation(.zero, in: self)
        
        let pt  = CGPoint(x: self.center.x+(translation.x * orientation), y: self.center.y+(translation.y * orientation))
        
        if pt.y > self.superview!.bounds.size.height || pt.y < 0 || pt.x < 0 || pt.x > self.superview!.bounds.size.width{
            return
        }
        
        self.center = pt
        if recognizer.state == .began{
            self.superview?.bringSubview(toFront: self)
        }
        if recognizer.state == .ended || recognizer.state == .cancelled {
            self.dropView()
        }
    }
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        
        self.draggView(recognizer)
      
    }
    
    
    func dropView(){

        for key in Array(squareBounds.keys){
            guard let rct = squareBounds[key] else{
                continue
            }
            if rct.contains(self.center){
                
                let actionType = piece!.validateNewSquare(squareKey: key)
                
                if actionType == .move{
                    piece!.move(squares[key]!)
                    piece!.updateAfterMove(squareId: key)
//                    return
                }else if actionType == .attack{
                    piece!.attack(squares[key]!)
                    piece!.updateAfterMove(squareId: key)
//                    return
                }else if actionType == .castling{
                    (piece as! King).doCastling()
                    (piece as! King).shouldCastle!()
                    (piece as! King).updateAfterCastle()
//                    return
                }
                break
            }
        }
        let frm = self.piece?.currentSquare.frame
        self.center = CGPoint(x: frm!.midX, y: frm!.midY)
    }
  
}
