//
//  BoardView.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/3/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import UIKit

class BoardView: UIImageView {

    var rotated:Bool = false
    let board:Board = Board()

    func rotate(){
        
        if !rotated{
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            board.orientation = .upsideDown
            for view in self.subviews{
                if view is PieceView{
                    (view as! PieceView).rotate()
                    (view as! PieceView).piece?.boardOrientation = .upsideDown
                }
            }
            
        }else{
            self.transform = CGAffineTransform.identity
            board.orientation = .upsideDown
            for view in self.subviews{
                if view is PieceView{
                    (view as! PieceView).identity()
                    (view as! PieceView).piece?.boardOrientation = .upside
                }
            }
        }
        rotated = !rotated
    }
    
    func calculateThePointsForSquars(){
        
        let adjustedWidth = Int(self.bounds.size.width / 8) * 8
        let margin = (Int(self.bounds.size.width) - adjustedWidth) / 2
        let squarSize = adjustedWidth / 8
        for squreId in squareIds{
            let square = squares[squreId]
            let y = (Int(self.bounds.size.height) - margin) - ((square!.location.1 + 1) * squarSize)
            let x = margin + (square!.location.0  * squarSize)
            let rect = CGRect(x: x, y: y, width: squarSize, height: squarSize)
            square?.frame = rect
            squareBounds.updateValue(rect, forKey: squreId)
            self.placePieces(square: square!)
        }
    }
    
    func placePieces(square:Square){
        
        let piece:Piece?
        let image:UIImage?
        switch square.squareId {
        case "A1":
            piece = Rook(pieceId: .rookWhiteLeft, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .rookWhiteLeft
            image = UIImage(named: "RookWhite")!
        case "B1":
            piece = Knight(pieceId: .knightWhiteLeft, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .knightWhiteLeft
            image = UIImage(named: "KnightWhite")!
        case "C1":
            piece = Bishop(pieceId: .bishopWhiteLeft, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .bishopWhiteLeft
            image = UIImage(named: "BishopWhite")!
        case "D1":
            piece = Queen(pieceId: .queenWhite, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .queenWhite
            image = UIImage(named: "QueenWhite")!
        case "E1":
            piece = King(pieceId: .kingWhite, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .kingWhite
            image = UIImage(named: "KingWhite")!
        case "F1":
            piece = Bishop(pieceId: .bishopWhiteRight, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .bishopWhiteRight
            image = UIImage(named: "BishopWhite")!
        case "G1":
            piece = Knight(pieceId: .knightWhiteRight, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .knightWhiteRight
            image = UIImage(named: "KnightWhite")!
        case "H1":
            piece = Rook(pieceId: .rookWhiteRight, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .rookWhiteRight
            image = UIImage(named: "RookWhite")!
        case "A8":
            piece = Rook(pieceId: .rookBlackLeft, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .rookBlackLeft
            image = UIImage(named: "RookBlack")!
            
        case "B8":
            piece = Knight(pieceId: .knightBlackLeft, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .knightBlackLeft
            image = UIImage(named: "KnightBlack")!
            
        case "C8":
            piece = Bishop(pieceId: .bishopBlackLeft, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .bishopBlackLeft
            image = UIImage(named: "BishopBlack")!
            
        case "D8":
            piece = Queen(pieceId: .queenBlack, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .queenBlack
            image = UIImage(named: "QueenBlack")!
            
        case "E8":
            piece = King(pieceId: .kingBlack, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .kingBlack
            image = UIImage(named: "KingBlack")!
            
        case "F8":
            piece = Bishop(pieceId: .bishopBlackRight, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .bishopBlackRight
            image = UIImage(named: "BishopBlack")!
            
        case "G8":
            piece = Knight(pieceId: .knightBlackRight, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .knightBlackRight
            image = UIImage(named: "KnightBlack")!
            
        case "H8":
            piece = Rook(pieceId: .rookBlackRight, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .rookBlackRight
            image = UIImage(named: "RookBlack")!
            
        case "A2":
            piece = Pawn(pieceId: .pawnWhiteA2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteA2
            image = UIImage(named: "PawnWhite")!
        case "B2":
            piece = Pawn(pieceId: .pawnWhiteB2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteB2
            image = UIImage(named: "PawnWhite")!
        case "C2":
            piece = Pawn(pieceId: .pawnWhiteC2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteC2
            image = UIImage(named: "PawnWhite")!
        case "D2":
            piece = Pawn(pieceId: .pawnWhiteD2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteD2
            image = UIImage(named: "PawnWhite")!
        case "E2":
            piece = Pawn(pieceId: .pawnWhiteE2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteE2
            image = UIImage(named: "PawnWhite")!
        case "F2":
            piece = Pawn(pieceId: .pawnWhiteF2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteF2
            image = UIImage(named: "PawnWhite")!
        case "G2":
            piece = Pawn(pieceId: .pawnWhiteG2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteG2
            image = UIImage(named: "PawnWhite")!
        case "H2":
            piece = Pawn(pieceId: .pawnWhiteH2, currentSquare: square)
            piece?.direction = .upside
            square.currentPieceId = .pawnWhiteH2
            image = UIImage(named: "PawnWhite")!
            
        case "A7":
            piece = Pawn(pieceId: .pawnBlackA7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackA7
            image = UIImage(named: "PawnBlack")!
            
        case "B7":
            piece = Pawn(pieceId: .pawnBlackB7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackB7
            image = UIImage(named: "PawnBlack")!
            
        case "C7":
            piece = Pawn(pieceId: .pawnBlackC7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackC7
            image = UIImage(named: "PawnBlack")!
            
        case "D7":
            piece = Pawn(pieceId: .pawnBlackD7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackD7
            image = UIImage(named: "PawnBlack")!
            
        case "E7":
            piece = Pawn(pieceId: .pawnBlackE7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackE7
            image = UIImage(named: "PawnBlack")!
            
        case "F7":
            piece = Pawn(pieceId: .pawnBlackF7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackF7
            image = UIImage(named: "PawnBlack")!
            
        case "G7":
            piece = Pawn(pieceId: .pawnBlackG7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackG7
            image = UIImage(named: "PawnBlack")!
            
        case "H7":
            piece = Pawn(pieceId: .pawnBlackH7, currentSquare: square)
            piece?.direction = .upsideDown
            square.currentPieceId = .pawnBlackH7
            image = UIImage(named: "PawnBlack")!
        default:
            piece = nil
            image = nil
        }
        
        if piece != nil {
            let pieceView = PieceView(frame: square.frame!, piece: piece!)
            self.addSubview(pieceView)
            pieces.updateValue(piece!, forKey: piece!.pieceId)
            if image != nil{
                pieceView.image = image
            }
            piece?.shouldRemovePiece = { pieceId in
                let tmpPiece = pieces[pieceId]
                for pceView in self.subviews{
                    if pceView is PieceView {
                        if (pceView as! PieceView).piece! == tmpPiece!{
                            pieces.removeValue(forKey: pieceId)
                            (pceView as! PieceView).removeFromSuperview()
                            break
                        }
                    }
                }
            }
            
            piece?.shouldCastle = {
                
                let rk = (piece as! King).rookForCastling
                
                var rkView:PieceView?
                var kngView:PieceView?
                
                var pceViewCount = 0
                for pceView in self.subviews{
                    if pceView is PieceView {
                        if (pceView as! PieceView).piece! == rk!{
                            rkView = pceView as? PieceView
                            pceViewCount += 1
                        }
                        if (pceView as! PieceView).piece! == piece!{
                            kngView = pceView as? PieceView
                            pceViewCount += 1
                        }
                        
                        if pceViewCount == 2 {
                            break
                        }
                    }
                }
                
                let rkFrm = (piece as! King).newRookSquare?.frame
                let nwRkCtr = CGPoint(x:(rkFrm?.midX)! , y:(rkFrm?.midY)!)

                let kgFrm = (piece as! King).newKingSquare?.frame
                let nwKgCtr = CGPoint(x:(kgFrm?.midX)! , y:(kgFrm?.midY)!)
                
                rkView?.center = nwRkCtr
                kngView?.center = nwKgCtr

                
            }
            
            
        }
    }
}
