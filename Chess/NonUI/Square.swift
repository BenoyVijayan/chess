//
//  Square.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/3/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit

class Square{
    
    var location:(Int,Int) = (-1,-1)
    let squareId:String
    let squareColor:ChessColor
    var currentPieceId:PieceId? = nil
    var frame:CGRect? = nil
  
    var left:String{
        get{
            if location.0 == 0 {
                return ""
            }
            let file = files[location.0 - 1]
            let rank = location.1 + 1
            return  "\(file)\(rank)"
        }
    }
    
    var leftSquare:Square?{
        get{
            return squares[left]
        }
    }
    
    var right:String{
        get{
            if location.0 == 7 {
                return ""
            }
            let file = files[location.0 + 1]
            let rank = location.1 + 1
            return  "\(file)\(rank)"
        }
    }
    
    var rightSquare:Square?{
        get{
            return squares[right]
        }
    }
    
    var up:String{
        get{
            if location.1 == 7 {
                return ""
            }
            let file = files[location.0 ]
            let rank = location.1 + 2
            return  "\(file)\(rank)"
        }
    }
    
    var upSquare:Square?{
        get{
            return squares[up]
        }
    }
    
    
    var down:String{
        get{
            if location.1 == 0 {
                return ""
            }
            let file = files[location.0 ]
            let rank = location.1
            return  "\(file)\(rank)"
        }
    }
    
    var downSquare:Square?{
        get{
            return squares[down]
        }
    }
    
    var upLeft:String {
        get{
            if location.0 == 0 || location.1 == 7  {
                return ""
            }
            let file = files[location.0  - 1]
            let rank = location.1 + 2
            return  "\(file)\(rank)"
        }
    }
    
    var upLeftSquare:Square?{
        get{
            return squares[upLeft]
        }
    }
    
    var upRight:String{
        get{
            if location.0 == 7 || location.1 == 7  {
                return ""
            }
            let file = files[location.0  + 1]
            let rank = location.1 + 2
            return  "\(file)\(rank)"
        }
    }
    
    var upRightSquare:Square?{
        get{
            return squares[upRight]
        }
    }
    
    var downLeft:String {
        get{
            if location.0 == 0 || location.1 == 0  {
                return ""
            }
            let file = files[location.0  - 1]
            let rank = location.1
            return  "\(file)\(rank)"
        }
    }
    
    var downLeftSquare:Square?{
        get{
            return squares[downLeft]
        }
    }
    
    var downRight:String {
        get{
            if location.0 == 7 || location.1 == 0  {
                return ""
            }
            let file = files[location.0  + 1]
            let rank = location.1
            return  "\(file)\(rank)"
        }
    }
    
    var downRightSquare:Square?{
        get{
            return squares[downRight]
        }
    }

    var ktUpLeft:String {
        get{
            if location.0 == 0 || location.1 > 5  {
                return ""
            }
            let file = files[location.0  - 1]
            let rank = location.1 + 3
            return  "\(file)\(rank)"
        }
    }
    
    var ktUpLeftSquare:Square?{
        get{
            return squares[ktUpLeft]
        }
    }
    
    var ktUpRight:String {
        get{
            if location.0 == 7 || location.1 > 5  {
                return ""
            }
            let file = files[location.0  + 1]
            let rank = location.1 + 3
            return  "\(file)\(rank)"
        }
    }
    
    var ktUpRightSquare:Square?{
        get{
            return squares[ktUpRight]
        }
    }
    
    var ktRightUp:String {
        get{
            if location.0 > 5 || location.1 == 7  {
                return ""
            }
            let file = files[location.0  + 2]
            let rank = location.1 + 2
            return  "\(file)\(rank)"
        }
    }
    
    var ktRightUpSquare:Square?{
        get{
            return squares[ktRightUp]
        }
    }
    
    var ktRightDown:String {
        get{
            if location.0 > 5 || location.1 == 0  {
                return ""
            }
            let file = files[location.0  + 2]
            let rank = location.1
            return  "\(file)\(rank)"
        }
    }
    
    var ktRightDownSquare:Square?{
        get{
            return squares[ktRightDown]
        }
    }
    
    var ktDownRight:String {
        get{
            if location.0 == 7 || location.1 < 2  {
                return ""
            }
            let file = files[location.0  + 1]
            let rank = location.1 - 1
            return  "\(file)\(rank)"
        }
    }
    
    var ktDownRightSquare:Square?{
        get{
            return squares[ktDownRight]
        }
    }
    
    var ktDownLeft:String {
        get{
            if location.0 == 0 || location.1 < 2  {
                return ""
            }
            let file = files[location.0  - 1]
            let rank = location.1 - 1
            return  "\(file)\(rank)"
        }
    }
    
    var ktDownLeftSquare:Square?{
        get{
            return squares[ktDownLeft]
        }
    }
    
    var ktLeftUp:String {
        get{
            if location.0 < 2 || location.1 == 7  {
                return ""
            }
            let file = files[location.0  - 2]
            let rank = location.1 + 2
            return  "\(file)\(rank)"
        }
    }
    
    var ktLeftUpSquare:Square?{
        get{
            return squares[ktLeftUp]
        }
    }
    
    var ktLeftDown:String {
        get{
            if location.0 < 2 || location.1 == 0  {
                return ""
            }
            let file = files[location.0  - 2]
            let rank = location.1
            return  "\(file)\(rank)"
        }
    }
    
    var ktLeftDownSquare:Square?{
        get{
            return squares[ktLeftDown]
        }
    }
    
    init(id iSquareId:String , color iSquareColor:ChessColor){
        self.squareId = iSquareId
        let file = iSquareId[0]
        let rank = Int(iSquareId[1])!
        location.0 = files.index(of: file)!
        location.1 = rank - 1
        self.squareColor = iSquareColor
    }
    
    public static func ==(left:Square,right:Square)->Bool{
        return left.squareId == right.squareId
    }
    
    public static func !=(left:Square,right:Square)->Bool{
        return left.squareId != right.squareId
    }
    
    func getOponentThretForPiece( _ iPice:Piece )->Bool{
    
        let opntPieces:[Piece] = pieces.values.filter{ return $0.color != iPice.color }
        for pc in opntPieces{
            if  pc.checkReachabilitySquare(self) == true{
                return true
            }
        }
        return false
    }

    var isOponentReachable:Bool{
        get{
            let opntPieces:[Piece] = pieces.values.filter{ return $0.color == self.currentPieceId?.color }
            for pc in opntPieces{
                if  pc.checkReachabilitySquare(self) == true{
                    return true
                }
            }
            
            return false
        }
    }
    
    public static func ~(left:Square,right:Square)->Movement{
    
        let deltaX = right.location.0 - left.location.0
        let deltaY = right.location.1 - left.location.1
        
        if deltaX == 0 && deltaY != deltaX{
            return deltaY < 0 ? .down : .up
        }else if deltaY == 0 && deltaY != deltaX{
            return deltaX < 0 ? .left : .right
        }else if abs(deltaX) == abs(deltaY){
            if deltaX > 0 && deltaY > 0{
                return .upRight
            }else if deltaX < 0  && deltaY > 0{
                return .upLeft
            }else if deltaX < 0  && deltaY < 0{
                return .downLeft
            }else if deltaX > 0  && deltaY < 0{
                return .downRight
            }
        }else if  deltaX == 1  && deltaY == 2{
            return .ktUpRight
        }else if deltaX == -1  && deltaY == 2{
            return .ktUpLeft
        }else if deltaX == 1  && deltaY == -2{
            return .ktDownRight
        }else if deltaX == -1  && deltaY == -2{
            return .ktDownLeft
        }else if deltaX == -2  && deltaY == 1{
            return .ktLeftUp
        }else if deltaX == -2  && deltaY == -1{
            return .ktLeftDown
        }else if deltaX == 2  && deltaY == 1{
            return .ktRightUp
        }else if deltaX == 2  && deltaY == -1{
            return .ktRightDown
        }
        
        return .invalid
    }
}

