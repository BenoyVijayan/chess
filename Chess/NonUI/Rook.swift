//
//  Rook.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/5/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

class Rook:Piece{
    
    var isFirstMove:Bool = true
    
    override func validateMove( _ square:Square)->ActionType{

        let move = self.currentSquare ~ square
        if !move.isStraight{
            return  .invalid
        }
        var tempSquar:Square? = self.currentSquare
        while tempSquar != nil && tempSquar! != square{
            
            switch move {
            case .up:
                tempSquar = tempSquar!.upSquare
            case .down:
                tempSquar = tempSquar!.downSquare
            case .left:
                tempSquar = tempSquar!.leftSquare
            case .right:
                tempSquar = tempSquar!.rightSquare
            default:
                return .invalid
            }
            
            if tempSquar! != square &&  tempSquar!.currentPieceId != nil {
                return .invalid
            }
            
        }
        
        if tempSquar! == square{
            
            if square.currentPieceId == nil{
                isFirstMove = false
                return .move
            }else{
                let destPc = pieces[square.currentPieceId!]
                if destPc?.color != self.color {
                    isFirstMove = false
                    return .attack
                }
            }
        }
        
        return .invalid
    }
    
    override func checkReachabilitySquare(_ iSquare:Square)->Bool{
        
        var tempSquare:Square? = self.currentSquare.upSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.upSquare
        }
        
        tempSquare = self.currentSquare.rightSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.rightSquare
        }
        
        tempSquare = self.currentSquare.downSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.downSquare
        }
        
        tempSquare = self.currentSquare.leftSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.leftSquare
        }
        
        return false
    }
}
