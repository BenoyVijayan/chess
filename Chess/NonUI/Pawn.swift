//
//  Pawn.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/4/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

class Pawn:Piece{
   
    /*This is required for pawn as the first move can be two square */
    var isFirstMove:Bool = true
   
    /* Validating the pawn movement towards up direction */
    private func validateForUpSidePawnMove( _ square:Square)->ActionType{
        
        /*  Getting next square in up direction
                If that is nil returns false  */
        guard let sqrUp1 = currentSquare.upSquare else{
            
            return .invalid
        }
        
        /*      Checking if the first square in the up direction is the detination &
                if that does not have any piece         */
        if sqrUp1 == square && sqrUp1.currentPieceId == nil{
            
            return .move
        }
        
        /*      Getting second square in the up direction
                If that is nil returns false        */
        guard let sqrUp2 = sqrUp1.upSquare else{
            
            return .invalid
        }
        
        /*      Checking if the second square is the destination &
                if that does not have any piece &
                if first square also does not have any piece &
                checkin if this is the first move/jump of the pawn  */
        if sqrUp2 == square && sqrUp2.currentPieceId == nil &&
            sqrUp1.currentPieceId == nil && isFirstMove == true  {
            
            return .move
        }
        
        return .invalid
    }
    
    private func validateForUpSidePawnAttack( _ square:Square)->ActionType{
        
        /* Getting the both the squares (up left & up right) */
        let sqrUpLeft = currentSquare.upLeftSquare
        let sqrUpRight = currentSquare.upRightSquare
        
        
        if sqrUpLeft != nil && sqrUpLeft! == square { /* Checking if upleft square is the destination */
            
            
            if  square.currentPieceId != nil{ /* Checking if up-left has any peice */
                let destPiece = pieces[square.currentPieceId!]
                
                /* return true if both the pieces are different in color else returns false */
                if  destPiece?.color != self.color{
                    return .attack
                }
            }
        }else if sqrUpRight != nil && sqrUpRight! == square{ /* Checking if up-right square is the destination */
            
            
            if  square.currentPieceId != nil{ /* Checking if up-right has any peice */
                let destPiece = pieces[square.currentPieceId!]
                
                /* return true if both the pieces are different in color else returns false */
                if  destPiece?.color != self.color{
                    return .attack
                }
            }
        }else{
            return .invalid
        }
        
        
        return .invalid
    }
    
    private func validateForUpSideDownPawnMove(_ square:Square)->ActionType{

        /*  Getting next square in up direction
         If that is nil returns false  */
        guard let sqrUp1 = currentSquare.downSquare else{
            
            return .invalid
        }
        
        /*      Checking if the first square in the up direction is the detination &
         if that does not have any piece         */
        if sqrUp1 == square && sqrUp1.currentPieceId == nil{
            
            return .move
        }
        
        /*      Getting second square in the up direction
         If that is nil returns false        */
        guard let sqrUp2 = sqrUp1.downSquare else{
            
            return .invalid
        }
        
        /*      Checking if the second square is the destination &
         if that does not have any piece &
         if first square also does not have any piece &
         checkin if this is the first move/jump of the pawn  */
        if sqrUp2 == square && sqrUp2.currentPieceId == nil &&
            sqrUp1.currentPieceId == nil && isFirstMove == true  {
            
            return .move
        }
        
        return .invalid
    }
    
    private func validateForUpSideDownPawnAttack( _ square:Square)->ActionType{
        /* Getting the both the squares (up left & up right) */
        let sqrDownLeft = currentSquare.downLeftSquare
        let sqrDownRight = currentSquare.downRightSquare
        
        
        if sqrDownLeft != nil && sqrDownLeft! == square { /* Checking if down-left square is the destination */
            
            
            if  square.currentPieceId != nil{ /* Checking if down-left has any peice */
                let destPiece = pieces[square.currentPieceId!]
                
                /* return true if both the pieces are different in color else returns false */
                if  destPiece?.color != self.color{
                    return .attack
                }

            }
        }else if sqrDownRight != nil && sqrDownRight! == square{ /* Checking if down-right square is the destination */
            
            
            if  square.currentPieceId != nil{ /* Checking if down-right has any peice */
                let destPiece = pieces[square.currentPieceId!]
                
                /* return true if both the pieces are different in color else returns false */
                if  destPiece?.color != self.color{
                    return .attack
                }
            }
        }else{
            return .invalid
        }

        return .invalid
    }
    
    override func checkReachabilitySquare(_ iSquare:Square)->Bool{
        
        if direction == .upside{
            var tempSquare:Square? = currentSquare.upLeftSquare
            if tempSquare != nil{
                if iSquare == tempSquare! {
                    return true
                }
            }
            
            tempSquare = currentSquare.upRightSquare
            if tempSquare != nil{
                if iSquare == tempSquare! {
                    return true
                }
            }
            
        }else if direction == .upsideDown{
            
            var tempSquare:Square? = currentSquare.downLeftSquare
            if tempSquare != nil{
                if iSquare == tempSquare! {
                    return true
                }
            }
            
            tempSquare = currentSquare.downRightSquare
            if tempSquare != nil{
                if iSquare == tempSquare! {
                    return true
                }
            }
            
        }
        
        return false
    }
    
    override func validateNewSquare( squareKey:String)->ActionType{
        
        guard let destSquare = squares[squareKey] else{
            return .invalid
        }
        
        if direction == .upside{
            if validateForUpSidePawnMove(destSquare) == .move{
                return .move
            }else if validateForUpSidePawnAttack(destSquare) == .attack{
                return .attack
            }
        }else if direction == .upsideDown{
            if validateForUpSideDownPawnMove(destSquare) == .move{
                return .move
            }else if validateForUpSideDownPawnAttack(destSquare) == .attack{
                return .attack
            }
        }
        
        return .invalid
    }
    
    override func  updateAfterMove(squareId:String){
        isFirstMove = false
        super.updateAfterMove(squareId: squareId)
    }
    
}
