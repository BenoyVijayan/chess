//
//  Globals.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/3/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit

infix operator ~: AdditionPrecedence


public let files = ["A","B","C","D","E","F","G","H"]

public let squareIds = ["A1","B1","C1","D1","E1","F1","G1","H1",
                        "A2","B2","C2","D2","E2","F2","G2","H2",
                        "A3","B3","C3","D3","E3","F3","G3","H3",
                        "A4","B4","C4","D4","E4","F4","G4","H4",
                        "A5","B5","C5","D5","E5","F5","G5","H5",
                        "A6","B6","C6","D6","E6","F6","G6","H6",
                        "A7","B7","C7","D7","E7","F7","G7","H7",
                        "A8","B8","C8","D8","E8","F8","G8","H8"
                        ]

var squareBounds:[String:CGRect] = [String:CGRect]()

var squares:[String : Square] = [String : Square]()

var pieces :[PieceId:Piece] = [PieceId:Piece]()



extension String{
    
    subscript(index:Int) -> String{
        
        if index >= self.count {
            
            return ""
        }
        let chars = Array(self)
        let str:String = String(chars[index])
        
        return str
    }
}

enum PieceId:Int{
    //White
    case kingWhite = 0
    case queenWhite = 1
    case bishopWhiteLeft = 2
    case bishopWhiteRight = 3
    case knightWhiteLeft  = 4
    case knightWhiteRight = 5
    case rookWhiteLeft = 6
    case rookWhiteRight = 7
    case pawnWhiteA2 = 8
    case pawnWhiteB2 = 9
    case pawnWhiteC2 = 10
    case pawnWhiteD2 = 11
    case pawnWhiteE2 = 12
    case pawnWhiteF2 = 13
    case pawnWhiteG2 = 14
    case pawnWhiteH2 = 16
    
    //Black
    case kingBlack = 17
    case queenBlack = 18
    case bishopBlackLeft = 19
    case bishopBlackRight = 20
    case knightBlackLeft = 21
    case knightBlackRight = 22
    case rookBlackLeft = 23
    case rookBlackRight = 24
    case pawnBlackA7 = 25
    case pawnBlackB7 = 26
    case pawnBlackC7 = 27
    case pawnBlackD7 = 28
    case pawnBlackE7 = 29
    case pawnBlackF7 = 30
    case pawnBlackG7 = 31
    case pawnBlackH7 = 32
    
    var color:ChessColor{
        get{
            return self.rawValue < 17 ? .chessWhite : .chessBlack
        }
    }
    
    var isPawn:Bool{
        get{
            if  (self.rawValue > 7 &&  self.rawValue < 17 ) ||  self.rawValue > 24{ return true }
            return false
        }
    }
    
    var isKnight:Bool{
        get{
            if  self.rawValue == 21 ||  self.rawValue == 22  ||  self.rawValue == 4 || self.rawValue == 5 {
                return true
            }
            return false
        }
    }
    
    var isKing:Bool{
        
        get{
            if  self.rawValue == 17 ||  self.rawValue == 0  {
                return true
            }
            return false
        }
    }
}


enum PieceDirection{
    case upside
    case upsideDown
}

enum ActionType{
    case invalid
    case move
    case attack
    case nPassent
    case castling
}

enum ChessColor{
    
    case chessBlack
    case chessWhite
}
var activeColor:ChessColor = .chessWhite
enum Movement{
    
    case up
    case down
    case right
    case left
    case upLeft
    case upRight
    case downRight
    case downLeft
    
    
    case ktUpRight
    case ktUpLeft
    case ktDownLeft
    case ktDownRight
    case ktRightUp
    case ktLeftUp
    case ktRightDown
    case ktLeftDown
    
    case invalid
    
    var isStraight:Bool{
        get{
            if  self == .up || self == .down || self == .left || self == .right {
                return true
            }
            return false
        }
    }
    
    var isDiagonal:Bool{
        get{
            if  self == .upLeft || self == .downLeft || self == .upRight || self == .downRight {
                return true
            }
            return false
            
        }
    }
    
    var isKnightMove:Bool{
        
        get{
            if  self == .ktUpLeft || self == .ktDownLeft || self == .ktUpRight || self == .ktDownRight ||
                self == .ktLeftUp || self == .ktLeftDown || self == .ktRightUp || self == .ktRightDown{
                return true
            }
            return false
            
        }
    }
    
}

