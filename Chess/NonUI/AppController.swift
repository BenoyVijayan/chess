//
//  AppController.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/2/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit
class AppController{

    private var window:UIWindow = UIWindow(frame: UIScreen.main.bounds)
    private var boardViewController:BoardViewController =  BoardViewController.instance
    static let instance:AppController = AppController()
    private init(){
        window.makeKeyAndVisible()
    }
    
    func showBoardViewControler(){
        window.rootViewController = boardViewController
    }
}
