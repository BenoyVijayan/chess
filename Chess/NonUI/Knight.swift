//
//  Knight.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/6/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

class Knight:Piece{
    
    override func validateMove( _ square:Square)->ActionType{
        
        let move = self.currentSquare ~ square
        
        if !move.isKnightMove{
            return .invalid
        }
        
        if square.currentPieceId == nil{
            return .move
        }else{
            let destPc = pieces[square.currentPieceId!]
            if destPc?.color != self.color {
                return .attack
            }
        }
        return .invalid
    }
    
    override func checkReachabilitySquare(_ iSquare:Square)->Bool{
        let move = self.currentSquare ~ iSquare
        return move.isKnightMove
    }
}
