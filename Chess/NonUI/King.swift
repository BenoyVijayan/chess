//
//  King.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/6/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

class King:Piece{
    
    var isFirstMove:Bool = true
    var rookForCastling:Rook? = nil
    var newKingSquare :Square?
    var newRookSquare :Square?
    var hasCheck:Bool{
        get{
            return self.currentSquare.isOponentReachable
        }
    }
    
    private func checkForCastling(_ square:Square, _ move:Movement)->ActionType{
        
        if isFirstMove == false{
            return .invalid
        }
        let deltaX = self.currentSquare.location.0 - square.location.0
        
        var rook:Rook?
        if abs(deltaX) == 2 {
            if deltaX < 0{
                let tempRook1:Rook = pieces[PieceId.rookBlackRight] as! Rook
                let tempRook2:Rook = pieces[PieceId.rookWhiteRight] as! Rook
                rook = self.direction == tempRook1.direction ? tempRook1 : tempRook2
                if !(rook?.isFirstMove)! { return .invalid}
            }else if deltaX > 0{
                let tempRook1 = pieces[PieceId.rookBlackLeft] as! Rook
                let tempRook2 = pieces[PieceId.rookWhiteLeft] as! Rook
                rook = self.direction == tempRook1.direction ? tempRook1 : tempRook2
                if !(rook?.isFirstMove)! { return .invalid}
            }
        }
        var tempSquar = self.currentSquare
        if tempSquar.getOponentThretForPiece(self) {return .invalid}
        tempSquar = ((deltaX < 0) ? tempSquar.rightSquare :tempSquar.leftSquare)!
        while tempSquar != (rook?.currentSquare)! {
            if tempSquar.currentPieceId != nil{ return .invalid }
            if tempSquar.getOponentThretForPiece(self) {return .invalid}
            tempSquar = (deltaX<0 ? tempSquar.rightSquare :tempSquar.leftSquare)!
        }
        rookForCastling = rook
        newKingSquare = ((deltaX < 0) ? self.currentSquare.rightSquare?.rightSquare :self.currentSquare.leftSquare?.leftSquare)!
        newRookSquare = ((deltaX < 0) ? self.currentSquare.rightSquare :self.currentSquare.leftSquare)
        
        return .castling
    }
    
    func doCastling(){
        self.currentSquare.currentPieceId = nil
        newKingSquare?.currentPieceId = self.pieceId
        self.currentSquare = newKingSquare!
        
        rookForCastling?.currentSquare.currentPieceId = nil
        newRookSquare?.currentPieceId = rookForCastling?.pieceId
        rookForCastling?.currentSquare = newRookSquare!
        
    }
    
    func updateAfterCastle(){
        
        self.currentSquare.currentPieceId = nil
        newKingSquare!.currentPieceId = self.pieceId
        rookForCastling!.currentSquare.currentPieceId = nil
        rookForCastling!.currentSquare = newRookSquare!
        rookForCastling!.currentSquare.currentPieceId = rookForCastling?.pieceId
        for pce in pieces{
            if pce.value.direction == self.direction{
                pce.value.disablePieceView!(true)
            }else{
                pce.value.disablePieceView!(false)
            }
        }
    }
    
    override func validateMove( _ square:Square)->ActionType{
        
        let move = self.currentSquare ~ square
        if !move.isStraight && !move.isDiagonal{
            return  .invalid
        }
        var tempSquar:Square? = self.currentSquare
        switch move {
            case .up:
                tempSquar = tempSquar!.upSquare
            case .down:
                tempSquar = tempSquar!.downSquare
            case .left:
                tempSquar = tempSquar!.leftSquare
            case .right:
                tempSquar = tempSquar!.rightSquare
            case .upRight:
                tempSquar = tempSquar!.upRightSquare
            case .upLeft:
                tempSquar = tempSquar!.upLeftSquare
            case .downLeft:
                tempSquar = tempSquar!.downLeftSquare
            case .downRight:
                tempSquar = tempSquar!.downRightSquare
            default:
                return .invalid
        }
     
        if tempSquar! == square{
            
            if square.isOponentReachable {
                return .invalid
            }
            if square.currentPieceId == nil{
                isFirstMove = false
                return .move
            }else {
                let destPc = pieces[square.currentPieceId!]
                if destPc?.color != self.color {
                    isFirstMove = false
                    return .attack
                }
            }
        }
        
        return checkForCastling(square, move)
    }
   
    override func checkReachabilitySquare(_ iSquare:Square)->Bool{
        
        var tempSquare:Square? = nil
        for i in 1...8{
            switch i{
                case 1:
                    tempSquare = currentSquare.upSquare
                case 2:
                   tempSquare = currentSquare.downSquare
                case 3:
                    tempSquare = currentSquare.leftSquare
                case 4:
                    tempSquare = currentSquare.rightSquare
                case 5:
                    tempSquare = currentSquare.upLeftSquare
                case 6:
                    tempSquare = currentSquare.upRightSquare
                case 7:
                    tempSquare = currentSquare.downLeftSquare
                case 8:
                    tempSquare = currentSquare.downRightSquare
                default:
                    return false
            }
            if tempSquare != nil{
                if tempSquare! == iSquare{ return true }
            }
        }
    
        return false
    }
}
