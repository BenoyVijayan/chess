//
//  Bishop.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/5/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

class Bishop:Piece{
    
    
    
    override func validateMove( _ square:Square)->ActionType{
        
        let diagonal = self.currentSquare ~ square
        
        if !diagonal.isDiagonal{
           return .invalid
        }
        
        var tempSquar:Square? = self.currentSquare
        while tempSquar != nil && tempSquar! != square{
            
            switch diagonal {
                case .upRight:
                        tempSquar = tempSquar!.upRightSquare
                case .upLeft:
                        tempSquar = tempSquar!.upLeftSquare
                case .downLeft:
                    tempSquar = tempSquar!.downLeftSquare
                case .downRight:
                    tempSquar = tempSquar!.downRightSquare
                default:
                    return .invalid
            
            }
            if tempSquar! != square &&  tempSquar!.currentPieceId != nil {
                return .invalid
            }
            
        }
        
        if tempSquar! == square{
            
            if square.currentPieceId == nil{
                return .move
            }else{
                let destPc = pieces[square.currentPieceId!]
                if destPc?.color != self.color {
                    return .attack
                }
            }
        }
      
      return .invalid
    }
    
    override func checkReachabilitySquare(_ iSquare:Square)->Bool{
        
        var tempSquare:Square? = self.currentSquare.upLeftSquare
        while tempSquare != nil{
            
            if tempSquare! == iSquare{
                return true
            }
            if tempSquare?.currentPieceId != nil{
                break
            }
            tempSquare = tempSquare!.upLeftSquare
        }
        
        tempSquare = self.currentSquare.upRightSquare
        while tempSquare != nil{
            
           
            if tempSquare! == iSquare{
                return true
            }

            if tempSquare?.currentPieceId != nil{
                break
            }
             tempSquare = tempSquare!.upRightSquare
        }
        
        tempSquare = self.currentSquare.downRightSquare
        while tempSquare != nil{
           
           
            
            if tempSquare! == iSquare{
                return true
                
            }
            if tempSquare?.currentPieceId != nil{
                break
            }
            tempSquare = tempSquare!.downRightSquare
        }
        
        tempSquare = self.currentSquare.downLeftSquare
        while tempSquare != nil{
            
            if tempSquare! == iSquare{
                return true
                
            }
            if tempSquare?.currentPieceId != nil{
                break
                
            }
            tempSquare = tempSquare!.downLeftSquare
        }
     
        return false
    }
}
