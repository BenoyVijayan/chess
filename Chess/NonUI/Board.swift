//
//  Board.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/2/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit

enum BoardOrientation{
    case upside
    case upsideDown
}

class Board{
    var orientation:BoardOrientation = .upside
    init(){
        var isWhite:Bool = false
        for id in squareIds{
            let color:ChessColor = isWhite ? .chessWhite : .chessBlack
            let square = Square(id: id, color: color)
            squares.updateValue(square, forKey: id)
            isWhite = !isWhite
        }
    }
}


