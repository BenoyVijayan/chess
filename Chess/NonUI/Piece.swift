//
//  Piece.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/3/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation
import UIKit


class Piece{
    
    var currentSquare:Square
    let pieceId:PieceId
    var boardOrientation:BoardOrientation = .upside
    var direction:PieceDirection = .upside
    var color:ChessColor{ get{ return self.pieceId.color } }
    
    init(pieceId iPieceId:PieceId ,currentSquare iCurrentSquare:Square){
        currentSquare = iCurrentSquare
        pieceId = iPieceId
    }
    
    func checkReachabilitySquare(_ iSquare:Square)->Bool{  return false }
    
    func validateNewSquare( squareKey:String)->ActionType{
        guard let destSquare = squares[squareKey] else{
            return .invalid
        }
        
        let retVal = self.validateMove(destSquare)
        
//        let myKing:King = self.color == .chessBlack ? pieces[.kingBlack] as! King:  pieces[.kingWhite] as! King
//        
//        if myKing.hasCheck == true {
//            return .invalid
//        }
        
        return retVal
    }
    
    func move(_ iSquare:Square){
        iSquare.currentPieceId = self.currentSquare.currentPieceId
        self.currentSquare.currentPieceId = nil
        self.currentSquare = iSquare
    }
    
    func attack(_ iSquare:Square){
        self.shouldRemovePiece!(iSquare.currentPieceId!)
    }
    
    func validateMove( _ square:Square)->ActionType{ return .invalid }
    
    func updateAfterMove(squareId:String){
        self.currentSquare.currentPieceId = nil
        self.currentSquare = squares[squareId]!
        self.currentSquare.currentPieceId = self.pieceId
        activeColor = activeColor == .chessBlack ? .chessWhite : .chessBlack
        for pce in pieces{
            if pce.value.direction == self.direction{
                pce.value.disablePieceView!(true)
            }else{
                pce.value.disablePieceView!(false)
                activeColor = pce.key.color
            }
        }
    }
    
    public static func ==(left:Piece,right:Piece)->Bool{
        return left.pieceId == right.pieceId
    }
    

    
    var shouldRemovePiece:((PieceId)->())?
    var shouldCastle:(()->())?
    var disablePieceView:((Bool)->())?
}

