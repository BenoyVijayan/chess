//
//  Queen.swift
//  Chess
//
//  Created by Binoy Vijayan on 4/6/18.
//  Copyright © 2018 Binoy Vijayan. All rights reserved.
//

import Foundation

class Queen:Piece{
    
   
    
    override func validateMove( _ square:Square)->ActionType{
        
        let move = self.currentSquare ~ square
        if !move.isStraight && !move.isDiagonal{
            return  .invalid
        }
        var tempSquar:Square? = self.currentSquare
        while tempSquar != nil && tempSquar! != square{
            switch move {
                case .up:
                    tempSquar = tempSquar!.upSquare
                case .down:
                    tempSquar = tempSquar!.downSquare
                case .left:
                    tempSquar = tempSquar!.leftSquare
                case .right:
                    tempSquar = tempSquar!.rightSquare
                case .upRight:
                    tempSquar = tempSquar!.upRightSquare
                case .upLeft:
                    tempSquar = tempSquar!.upLeftSquare
                case .downLeft:
                    tempSquar = tempSquar!.downLeftSquare
                case .downRight:
                    tempSquar = tempSquar!.downRightSquare
                default:
                    return .invalid
            }
            if tempSquar! != square &&  tempSquar!.currentPieceId != nil {
                return .invalid
            }
        }
        
        if tempSquar! == square{
            if square.currentPieceId == nil{
                return .move
            }else{
                let destPc = pieces[square.currentPieceId!]
                if destPc?.color != self.color {
                    return .attack
                }
            }
        }
        
        return .invalid
    }
    
    override func checkReachabilitySquare(_ iSquare:Square)->Bool{
        
        var tempSquare:Square? = self.currentSquare.upLeftSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.upLeftSquare
        }
        
        tempSquare = self.currentSquare.upRightSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.upRightSquare
        }
        
        tempSquare = self.currentSquare.downRightSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.downRightSquare
        }
        
        tempSquare = self.currentSquare.downLeftSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.downLeftSquare
        }
        tempSquare = self.currentSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.upSquare
        }
        
        tempSquare = self.currentSquare.rightSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.rightSquare
        }
        
        tempSquare = self.currentSquare.downSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.downSquare
        }
        
        tempSquare = self.currentSquare.leftSquare
        while tempSquare != nil{
            if tempSquare! == iSquare{ return true }
            if tempSquare?.currentPieceId != nil{ break }
            tempSquare = tempSquare!.leftSquare
        }
        
        return false
    }
    
    
}
